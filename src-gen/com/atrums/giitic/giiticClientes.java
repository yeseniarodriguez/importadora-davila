/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.giitic;

import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity giitic_clientes (stored in table giitic_clientes).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class giiticClientes extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "giitic_clientes";
    public static final String ENTITY_NAME = "giitic_clientes";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_CODIGO = "codigo";
    public static final String PROPERTY_NOMBRE = "nombre";
    public static final String PROPERTY_NOMBRELEGAL = "nombrelegal";
    public static final String PROPERTY_ID11 = "id11";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_GRUPOA = "grupoA";
    public static final String PROPERTY_CUPOTOTAL = "cupototal";
    public static final String PROPERTY_CUPODISPONIBLE = "cupodisponible";
    public static final String PROPERTY_LISTA = "lista";
    public static final String PROPERTY_LISTANOMBRE = "listaNombre";

    public giiticClientes() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getCodigo() {
        return (String) get(PROPERTY_CODIGO);
    }

    public void setCodigo(String codigo) {
        set(PROPERTY_CODIGO, codigo);
    }

    public String getNombre() {
        return (String) get(PROPERTY_NOMBRE);
    }

    public void setNombre(String nombre) {
        set(PROPERTY_NOMBRE, nombre);
    }

    public String getNombrelegal() {
        return (String) get(PROPERTY_NOMBRELEGAL);
    }

    public void setNombrelegal(String nombrelegal) {
        set(PROPERTY_NOMBRELEGAL, nombrelegal);
    }

    public String getId11() {
        return (String) get(PROPERTY_ID11);
    }

    public void setId11(String id11) {
        set(PROPERTY_ID11, id11);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public String getGrupoA() {
        return (String) get(PROPERTY_GRUPOA);
    }

    public void setGrupoA(String grupoA) {
        set(PROPERTY_GRUPOA, grupoA);
    }

    public Long getCupototal() {
        return (Long) get(PROPERTY_CUPOTOTAL);
    }

    public void setCupototal(Long cupototal) {
        set(PROPERTY_CUPOTOTAL, cupototal);
    }

    public Long getCupodisponible() {
        return (Long) get(PROPERTY_CUPODISPONIBLE);
    }

    public void setCupodisponible(Long cupodisponible) {
        set(PROPERTY_CUPODISPONIBLE, cupodisponible);
    }

    public String getLista() {
        return (String) get(PROPERTY_LISTA);
    }

    public void setLista(String lista) {
        set(PROPERTY_LISTA, lista);
    }

    public String getListaNombre() {
        return (String) get(PROPERTY_LISTANOMBRE);
    }

    public void setListaNombre(String listaNombre) {
        set(PROPERTY_LISTANOMBRE, listaNombre);
    }

}
