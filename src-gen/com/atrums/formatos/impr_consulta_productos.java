/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.formatos;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.ProductCategory;
/**
 * Entity class for entity impr_consulta_productos (stored in table impr_consulta_productos).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class impr_consulta_productos extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "impr_consulta_productos";
    public static final String ENTITY_NAME = "impr_consulta_productos";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_CODIGO = "codigo";
    public static final String PROPERTY_COMMERCIALNAME = "commercialName";
    public static final String PROPERTY_DESCRIPTION = "description";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PRODUCTCATEGORY = "productCategory";
    public static final String PROPERTY_LISTADEADMINISTRATIVOS = "listaDeAdministrativos";
    public static final String PROPERTY_LISTACOMPRAS = "listaCompras";
    public static final String PROPERTY_PRECIOSESPEC1 = "preciosEspec1";
    public static final String PROPERTY_PRECIOSESPEC2 = "preciosEspec2";
    public static final String PROPERTY_PRECIOSESPEC3 = "preciosEspec3";
    public static final String PROPERTY_PRECIOSESPEC4 = "preciosEspec4";
    public static final String PROPERTY_PRECIOMAYORISTA = "precioMayorista";
    public static final String PROPERTY_PVP = "pVP";
    public static final String PROPERTY_BODEGA2 = "bodega2";
    public static final String PROPERTY_BODEGA3 = "bodega3";
    public static final String PROPERTY_BODEGAPRINCIPAL = "bodegaPrincipal";
    public static final String PROPERTY_BODEGAQUITO = "bodegaQuito";
    public static final String PROPERTY_BODEGARECE = "bodegaRece";
    public static final String PROPERTY_TOTALSTOCK = "totalStock";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_UPDATED = "updated";

    public impr_consulta_productos() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public String getCodigo() {
        return (String) get(PROPERTY_CODIGO);
    }

    public void setCodigo(String codigo) {
        set(PROPERTY_CODIGO, codigo);
    }

    public String getCommercialName() {
        return (String) get(PROPERTY_COMMERCIALNAME);
    }

    public void setCommercialName(String commercialName) {
        set(PROPERTY_COMMERCIALNAME, commercialName);
    }

    public String getDescription() {
        return (String) get(PROPERTY_DESCRIPTION);
    }

    public void setDescription(String description) {
        set(PROPERTY_DESCRIPTION, description);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public ProductCategory getProductCategory() {
        return (ProductCategory) get(PROPERTY_PRODUCTCATEGORY);
    }

    public void setProductCategory(ProductCategory productCategory) {
        set(PROPERTY_PRODUCTCATEGORY, productCategory);
    }

    public BigDecimal getListaDeAdministrativos() {
        return (BigDecimal) get(PROPERTY_LISTADEADMINISTRATIVOS);
    }

    public void setListaDeAdministrativos(BigDecimal listaDeAdministrativos) {
        set(PROPERTY_LISTADEADMINISTRATIVOS, listaDeAdministrativos);
    }

    public BigDecimal getListaCompras() {
        return (BigDecimal) get(PROPERTY_LISTACOMPRAS);
    }

    public void setListaCompras(BigDecimal listaCompras) {
        set(PROPERTY_LISTACOMPRAS, listaCompras);
    }

    public BigDecimal getPreciosEspec1() {
        return (BigDecimal) get(PROPERTY_PRECIOSESPEC1);
    }

    public void setPreciosEspec1(BigDecimal preciosEspec1) {
        set(PROPERTY_PRECIOSESPEC1, preciosEspec1);
    }

    public BigDecimal getPreciosEspec2() {
        return (BigDecimal) get(PROPERTY_PRECIOSESPEC2);
    }

    public void setPreciosEspec2(BigDecimal preciosEspec2) {
        set(PROPERTY_PRECIOSESPEC2, preciosEspec2);
    }

    public BigDecimal getPreciosEspec3() {
        return (BigDecimal) get(PROPERTY_PRECIOSESPEC3);
    }

    public void setPreciosEspec3(BigDecimal preciosEspec3) {
        set(PROPERTY_PRECIOSESPEC3, preciosEspec3);
    }

    public BigDecimal getPreciosEspec4() {
        return (BigDecimal) get(PROPERTY_PRECIOSESPEC4);
    }

    public void setPreciosEspec4(BigDecimal preciosEspec4) {
        set(PROPERTY_PRECIOSESPEC4, preciosEspec4);
    }

    public BigDecimal getPrecioMayorista() {
        return (BigDecimal) get(PROPERTY_PRECIOMAYORISTA);
    }

    public void setPrecioMayorista(BigDecimal precioMayorista) {
        set(PROPERTY_PRECIOMAYORISTA, precioMayorista);
    }

    public BigDecimal getPVP() {
        return (BigDecimal) get(PROPERTY_PVP);
    }

    public void setPVP(BigDecimal pVP) {
        set(PROPERTY_PVP, pVP);
    }

    public BigDecimal getBodega2() {
        return (BigDecimal) get(PROPERTY_BODEGA2);
    }

    public void setBodega2(BigDecimal bodega2) {
        set(PROPERTY_BODEGA2, bodega2);
    }

    public BigDecimal getBodega3() {
        return (BigDecimal) get(PROPERTY_BODEGA3);
    }

    public void setBodega3(BigDecimal bodega3) {
        set(PROPERTY_BODEGA3, bodega3);
    }

    public BigDecimal getBodegaPrincipal() {
        return (BigDecimal) get(PROPERTY_BODEGAPRINCIPAL);
    }

    public void setBodegaPrincipal(BigDecimal bodegaPrincipal) {
        set(PROPERTY_BODEGAPRINCIPAL, bodegaPrincipal);
    }

    public BigDecimal getBodegaQuito() {
        return (BigDecimal) get(PROPERTY_BODEGAQUITO);
    }

    public void setBodegaQuito(BigDecimal bodegaQuito) {
        set(PROPERTY_BODEGAQUITO, bodegaQuito);
    }

    public BigDecimal getBodegaRece() {
        return (BigDecimal) get(PROPERTY_BODEGARECE);
    }

    public void setBodegaRece(BigDecimal bodegaRece) {
        set(PROPERTY_BODEGARECE, bodegaRece);
    }

    public BigDecimal getTotalStock() {
        return (BigDecimal) get(PROPERTY_TOTALSTOCK);
    }

    public void setTotalStock(BigDecimal totalStock) {
        set(PROPERTY_TOTALSTOCK, totalStock);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

}
