/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.formatos;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity impr_comision (stored in table impr_comision).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class impr_comision extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "impr_comision";
    public static final String ENTITY_NAME = "impr_comision";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_TIPOCLIENTE = "tipoCliente";
    public static final String PROPERTY_TIPOCOMERCIAL = "tipoComercial";
    public static final String PROPERTY_TIPOCOMISION = "tipoComision";
    public static final String PROPERTY_RANGOUNO = "rangoUno";
    public static final String PROPERTY_RANGODOS = "rangoDos";
    public static final String PROPERTY_RANGOTRES = "rangoTres";
    public static final String PROPERTY_RANGOCUATRO = "rangoCuatro";
    public static final String PROPERTY_RANGOCINCO = "rangoCinco";
    public static final String PROPERTY_FECHAINICIO = "fechaInicio";
    public static final String PROPERTY_FECHAFIN = "fechaFin";

    public impr_comision() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_RANGOUNO, new BigDecimal(0));
        setDefaultValue(PROPERTY_RANGODOS, new BigDecimal(0));
        setDefaultValue(PROPERTY_RANGOTRES, new BigDecimal(0));
        setDefaultValue(PROPERTY_RANGOCUATRO, new BigDecimal(0));
        setDefaultValue(PROPERTY_RANGOCINCO, new BigDecimal(0));
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getTipoCliente() {
        return (String) get(PROPERTY_TIPOCLIENTE);
    }

    public void setTipoCliente(String tipoCliente) {
        set(PROPERTY_TIPOCLIENTE, tipoCliente);
    }

    public String getTipoComercial() {
        return (String) get(PROPERTY_TIPOCOMERCIAL);
    }

    public void setTipoComercial(String tipoComercial) {
        set(PROPERTY_TIPOCOMERCIAL, tipoComercial);
    }

    public String getTipoComision() {
        return (String) get(PROPERTY_TIPOCOMISION);
    }

    public void setTipoComision(String tipoComision) {
        set(PROPERTY_TIPOCOMISION, tipoComision);
    }

    public BigDecimal getRangoUno() {
        return (BigDecimal) get(PROPERTY_RANGOUNO);
    }

    public void setRangoUno(BigDecimal rangoUno) {
        set(PROPERTY_RANGOUNO, rangoUno);
    }

    public BigDecimal getRangoDos() {
        return (BigDecimal) get(PROPERTY_RANGODOS);
    }

    public void setRangoDos(BigDecimal rangoDos) {
        set(PROPERTY_RANGODOS, rangoDos);
    }

    public BigDecimal getRangoTres() {
        return (BigDecimal) get(PROPERTY_RANGOTRES);
    }

    public void setRangoTres(BigDecimal rangoTres) {
        set(PROPERTY_RANGOTRES, rangoTres);
    }

    public BigDecimal getRangoCuatro() {
        return (BigDecimal) get(PROPERTY_RANGOCUATRO);
    }

    public void setRangoCuatro(BigDecimal rangoCuatro) {
        set(PROPERTY_RANGOCUATRO, rangoCuatro);
    }

    public BigDecimal getRangoCinco() {
        return (BigDecimal) get(PROPERTY_RANGOCINCO);
    }

    public void setRangoCinco(BigDecimal rangoCinco) {
        set(PROPERTY_RANGOCINCO, rangoCinco);
    }

    public Date getFechaInicio() {
        return (Date) get(PROPERTY_FECHAINICIO);
    }

    public void setFechaInicio(Date fechaInicio) {
        set(PROPERTY_FECHAINICIO, fechaInicio);
    }

    public Date getFechaFin() {
        return (Date) get(PROPERTY_FECHAFIN);
    }

    public void setFechaFin(Date fechaFin) {
        set(PROPERTY_FECHAFIN, fechaFin);
    }

}
