//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.rdep.RetencionesEmpleado;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData implements FieldProvider {
static Logger log4j = Logger.getLogger(RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String isactive;
  public String anios;
  public String aniosr;
  public String generar;
  public String fechaInicio;
  public String fechaFin;
  public String generarXml;
  public String nombrerdep;
  public String adClientId;
  public String atrdepCabeceraRetenId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("anios"))
      return anios;
    else if (fieldName.equalsIgnoreCase("aniosr"))
      return aniosr;
    else if (fieldName.equalsIgnoreCase("generar"))
      return generar;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("generar_xml") || fieldName.equals("generarXml"))
      return generarXml;
    else if (fieldName.equalsIgnoreCase("nombrerdep"))
      return nombrerdep;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("atrdep_cabecera_reten_id") || fieldName.equals("atrdepCabeceraRetenId"))
      return atrdepCabeceraRetenId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atrdep_cabecera_reten.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atrdep_cabecera_reten.CreatedBy) as CreatedByR, " +
      "        to_char(atrdep_cabecera_reten.Updated, ?) as updated, " +
      "        to_char(atrdep_cabecera_reten.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atrdep_cabecera_reten.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atrdep_cabecera_reten.UpdatedBy) as UpdatedByR," +
      "        atrdep_cabecera_reten.AD_Org_ID, " +
      "(CASE WHEN atrdep_cabecera_reten.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(atrdep_cabecera_reten.Isactive, 'N') AS Isactive, " +
      "atrdep_cabecera_reten.Anios, " +
      "(CASE WHEN atrdep_cabecera_reten.Anios IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS AniosR, " +
      "atrdep_cabecera_reten.Generar, " +
      "atrdep_cabecera_reten.Fecha_Inicio, " +
      "atrdep_cabecera_reten.Fecha_Fin, " +
      "atrdep_cabecera_reten.Generar_Xml, " +
      "atrdep_cabecera_reten.Nombrerdep, " +
      "atrdep_cabecera_reten.AD_Client_ID, " +
      "atrdep_cabecera_reten.Atrdep_Cabecera_Reten_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atrdep_cabecera_reten left join (select AD_Org_ID, Name from AD_Org) table1 on (atrdep_cabecera_reten.AD_Org_ID = table1.AD_Org_ID) left join ad_ref_list_v list1 on (atrdep_cabecera_reten.Anios = list1.value and list1.ad_reference_id = 'DBE0CE97929A4EB3A2093E35875EA82B' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND atrdep_cabecera_reten.Atrdep_Cabecera_Reten_ID = ? " +
      "        AND atrdep_cabecera_reten.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atrdep_cabecera_reten.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData = new RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData();
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.created = UtilSql.getValue(result, "created");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.updated = UtilSql.getValue(result, "updated");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.updatedby = UtilSql.getValue(result, "updatedby");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.isactive = UtilSql.getValue(result, "isactive");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.anios = UtilSql.getValue(result, "anios");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.aniosr = UtilSql.getValue(result, "aniosr");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.generar = UtilSql.getValue(result, "generar");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.generarXml = UtilSql.getValue(result, "generar_xml");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.nombrerdep = UtilSql.getValue(result, "nombrerdep");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.atrdepCabeceraRetenId = UtilSql.getValue(result, "atrdep_cabecera_reten_id");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.language = UtilSql.getValue(result, "language");
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.adUserClient = "";
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.adOrgClient = "";
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.createdby = "";
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.trBgcolor = "";
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.totalCount = "";
        objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[] = new RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[vector.size()];
    vector.copyInto(objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData);
    return(objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData);
  }

/**
Create a registry
 */
  public static RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[] set(String updatedby, String updatedbyr, String createdby, String createdbyr, String generar, String fechaInicio, String fechaFin, String nombrerdep, String generarXml, String anios, String isactive, String atrdepCabeceraRetenId, String adOrgId, String adClientId)    throws ServletException {
    RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[] = new RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[1];
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0] = new RetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData();
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].created = "";
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].createdbyr = createdbyr;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].updated = "";
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].updatedTimeStamp = "";
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].updatedby = updatedby;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].updatedbyr = updatedbyr;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].adOrgId = adOrgId;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].adOrgIdr = "";
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].isactive = isactive;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].anios = anios;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].aniosr = "";
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].generar = generar;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].fechaInicio = fechaInicio;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].fechaFin = fechaFin;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].generarXml = generarXml;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].nombrerdep = nombrerdep;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].adClientId = adClientId;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].atrdepCabeceraRetenId = atrdepCabeceraRetenId;
    objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData[0].language = "";
    return objectRetencionesCabecera8982CE6EA9E8433E8FA23C11DEED4D6CData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef006C6816E5C04A05A81566B85E2117B1_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef148001D503F14A828878D763A65B76D2_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atrdep_cabecera_reten" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , Anios = (?) , Generar = (?) , Fecha_Inicio = TO_DATE(?) , Fecha_Fin = TO_DATE(?) , Generar_Xml = (?) , Nombrerdep = (?) , AD_Client_ID = (?) , Atrdep_Cabecera_Reten_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atrdep_cabecera_reten.Atrdep_Cabecera_Reten_ID = ? " +
      "        AND atrdep_cabecera_reten.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atrdep_cabecera_reten.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anios);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombrerdep);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrdepCabeceraRetenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrdepCabeceraRetenId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atrdep_cabecera_reten " +
      "        (AD_Org_ID, Isactive, Anios, Generar, Fecha_Inicio, Fecha_Fin, Generar_Xml, Nombrerdep, AD_Client_ID, Atrdep_Cabecera_Reten_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anios);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombrerdep);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atrdepCabeceraRetenId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atrdep_cabecera_reten" +
      "        WHERE atrdep_cabecera_reten.Atrdep_Cabecera_Reten_ID = ? " +
      "        AND atrdep_cabecera_reten.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atrdep_cabecera_reten.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atrdep_cabecera_reten" +
      "         WHERE atrdep_cabecera_reten.Atrdep_Cabecera_Reten_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atrdep_cabecera_reten" +
      "         WHERE atrdep_cabecera_reten.Atrdep_Cabecera_Reten_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
