//Sqlc generated V1.O00-1
package com.atrums.remica.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class SLOrderDocTypeValidacionData implements FieldProvider {
static Logger log4j = Logger.getLogger(SLOrderDocTypeValidacionData.class);
  private String InitRecordNumber="0";
  public String docsubtypeso;
  public String isdocnocontrolled;
  public String currentnext;
  public String currentnextsys;
  public String adSequenceId;
  public String issotrx;
  public String paymentrule;
  public String cPaymenttermId;
  public String invoicerule;
  public String deliveryrule;
  public String deliveryviarule;
  public String paymentrulepo;
  public String poPaymenttermId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("docsubtypeso"))
      return docsubtypeso;
    else if (fieldName.equalsIgnoreCase("isdocnocontrolled"))
      return isdocnocontrolled;
    else if (fieldName.equalsIgnoreCase("currentnext"))
      return currentnext;
    else if (fieldName.equalsIgnoreCase("currentnextsys"))
      return currentnextsys;
    else if (fieldName.equalsIgnoreCase("ad_sequence_id") || fieldName.equals("adSequenceId"))
      return adSequenceId;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("invoicerule"))
      return invoicerule;
    else if (fieldName.equalsIgnoreCase("deliveryrule"))
      return deliveryrule;
    else if (fieldName.equalsIgnoreCase("deliveryviarule"))
      return deliveryviarule;
    else if (fieldName.equalsIgnoreCase("paymentrulepo"))
      return paymentrulepo;
    else if (fieldName.equalsIgnoreCase("po_paymentterm_id") || fieldName.equals("poPaymenttermId"))
      return poPaymenttermId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static SLOrderDocTypeValidacionData[] select(ConnectionProvider connectionProvider, String cDoctypeId)    throws ServletException {
    return select(connectionProvider, cDoctypeId, 0, 0);
  }

  public static SLOrderDocTypeValidacionData[] select(ConnectionProvider connectionProvider, String cDoctypeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(d.DocSubTypeSO, '--') as DocSubTypeSO," +
      "        d.IsDocNoControlled, s.CurrentNext, s.CurrentNextSys, " +
      "        s.AD_Sequence_ID, d.IsSOTrx, " +
      "        '' as PaymentRule, '' as C_PaymentTerm_ID," +
      "        '' as InvoiceRule, '' as DeliveryRule," +
      "        '' as DeliveryViaRule," +
      "        '' as PaymentRulePO, '' as PO_PaymentTerm_ID" +
      "        FROM C_DocType d left join AD_Sequence s on d.DocNoSequence_ID=s.AD_Sequence_ID" +
      "        WHERE C_DocType_ID = ? ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SLOrderDocTypeValidacionData objectSLOrderDocTypeValidacionData = new SLOrderDocTypeValidacionData();
        objectSLOrderDocTypeValidacionData.docsubtypeso = UtilSql.getValue(result, "docsubtypeso");
        objectSLOrderDocTypeValidacionData.isdocnocontrolled = UtilSql.getValue(result, "isdocnocontrolled");
        objectSLOrderDocTypeValidacionData.currentnext = UtilSql.getValue(result, "currentnext");
        objectSLOrderDocTypeValidacionData.currentnextsys = UtilSql.getValue(result, "currentnextsys");
        objectSLOrderDocTypeValidacionData.adSequenceId = UtilSql.getValue(result, "ad_sequence_id");
        objectSLOrderDocTypeValidacionData.issotrx = UtilSql.getValue(result, "issotrx");
        objectSLOrderDocTypeValidacionData.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectSLOrderDocTypeValidacionData.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectSLOrderDocTypeValidacionData.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectSLOrderDocTypeValidacionData.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectSLOrderDocTypeValidacionData.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectSLOrderDocTypeValidacionData.paymentrulepo = UtilSql.getValue(result, "paymentrulepo");
        objectSLOrderDocTypeValidacionData.poPaymenttermId = UtilSql.getValue(result, "po_paymentterm_id");
        objectSLOrderDocTypeValidacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSLOrderDocTypeValidacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SLOrderDocTypeValidacionData objectSLOrderDocTypeValidacionData[] = new SLOrderDocTypeValidacionData[vector.size()];
    vector.copyInto(objectSLOrderDocTypeValidacionData);
    return(objectSLOrderDocTypeValidacionData);
  }

  public static SLOrderDocTypeValidacionData[] BPartner(ConnectionProvider connectionProvider, String cBpartnerId)    throws ServletException {
    return BPartner(connectionProvider, cBpartnerId, 0, 0);
  }

  public static SLOrderDocTypeValidacionData[] BPartner(ConnectionProvider connectionProvider, String cBpartnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT PaymentRule,C_PaymentTerm_ID," +
      "        InvoiceRule,DeliveryRule," +
      "        DeliveryViaRule," +
      "        PaymentRulePO,PO_PaymentTerm_ID" +
      "        FROM C_BPartner" +
      "        WHERE C_BPartner_ID=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SLOrderDocTypeValidacionData objectSLOrderDocTypeValidacionData = new SLOrderDocTypeValidacionData();
        objectSLOrderDocTypeValidacionData.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectSLOrderDocTypeValidacionData.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectSLOrderDocTypeValidacionData.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectSLOrderDocTypeValidacionData.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectSLOrderDocTypeValidacionData.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectSLOrderDocTypeValidacionData.paymentrulepo = UtilSql.getValue(result, "paymentrulepo");
        objectSLOrderDocTypeValidacionData.poPaymenttermId = UtilSql.getValue(result, "po_paymentterm_id");
        objectSLOrderDocTypeValidacionData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSLOrderDocTypeValidacionData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SLOrderDocTypeValidacionData objectSLOrderDocTypeValidacionData[] = new SLOrderDocTypeValidacionData[vector.size()];
    vector.copyInto(objectSLOrderDocTypeValidacionData);
    return(objectSLOrderDocTypeValidacionData);
  }

  public static String selectOldDocSubType(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT DOCSUBTYPESO FROM C_DOCTYPE" +
      "      WHERE C_DOCTYPE_ID IN (SELECT C_DOCTYPETARGET_ID FROM C_ORDER WHERE C_ORDER_ID = ?)";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "docsubtypeso");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectOldDocNo(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT DocumentNo FROM C_ORDER WHERE C_ORDER_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "documentno");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectOldDocTypeTargetId(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT c_doctypetarget_id FROM C_ORDER WHERE C_ORDER_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_doctypetarget_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
