//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class CODatosSRITipoDocData implements FieldProvider {
static Logger log4j = Logger.getLogger(CODatosSRITipoDocData.class);
  private String InitRecordNumber="0";
  public String coVencimientoAutSri;
  public String coNroAutSri;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("co_vencimiento_aut_sri") || fieldName.equals("coVencimientoAutSri"))
      return coVencimientoAutSri;
    else if (fieldName.equalsIgnoreCase("co_nro_aut_sri") || fieldName.equals("coNroAutSri"))
      return coNroAutSri;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CODatosSRITipoDocData[] SelectDatoSRI(ConnectionProvider connectionProvider, String cDocTypeId)    throws ServletException {
    return SelectDatoSRI(connectionProvider, cDocTypeId, 0, 0);
  }

  public static CODatosSRITipoDocData[] SelectDatoSRI(ConnectionProvider connectionProvider, String cDocTypeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select co_vencimiento_aut_sri, " +
      "               co_nro_aut_sri   " +
      "          from co_aut_sri " +
      "         where isactive = 'Y' and c_doctype_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDocTypeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CODatosSRITipoDocData objectCODatosSRITipoDocData = new CODatosSRITipoDocData();
        objectCODatosSRITipoDocData.coVencimientoAutSri = UtilSql.getDateValue(result, "co_vencimiento_aut_sri", "dd-MM-yyyy");
        objectCODatosSRITipoDocData.coNroAutSri = UtilSql.getValue(result, "co_nro_aut_sri");
        objectCODatosSRITipoDocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCODatosSRITipoDocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CODatosSRITipoDocData objectCODatosSRITipoDocData[] = new CODatosSRITipoDocData[vector.size()];
    vector.copyInto(objectCODatosSRITipoDocData);
    return(objectCODatosSRITipoDocData);
  }

  public static String ExisteDatoSRI(ConnectionProvider connectionProvider, String cDocTypeId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select count(*) " +
      "          from co_aut_sri " +
      "         where isactive = 'Y' and c_doctype_id = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDocTypeId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "count");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
