//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

class CORetencionVentaFacturaData implements FieldProvider {
static Logger log4j = Logger.getLogger(CORetencionVentaFacturaData.class);
  private String InitRecordNumber="0";
  public String round;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("round"))
      return round;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static String select(ConnectionProvider connectionProvider, String cDbBaseImp, String cBpRetencionVentaId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            select round(tr.porcentaje * to_number( ? ) / 100 ,2 )" +
      "              from co_bp_retencion_venta rv, " +
      "                   co_tipo_retencion tr" +
      "             where rv.co_tipo_retencion_id  = tr.co_tipo_retencion_id" +
      "               and rv.co_bp_retencion_venta_id = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDbBaseImp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpRetencionVentaId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "round");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
