//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_reports;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

class ReportReconciliationData implements FieldProvider {
static Logger log4j = Logger.getLogger(ReportReconciliationData.class);
  private String InitRecordNumber="0";
  public String resultado;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("resultado"))
      return resultado;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static String select(ConnectionProvider connectionProvider, String fin_financial_account_id, String dateacct)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select coalesce(sum(fa.amtsourcedr)-sum(fa.amtsourcecr),0) as resultado" +
      "      from fact_acct fa, fin_financial_account_acct f" +
      "      where fa.account_id = (select account_id from c_validcombination where c_validcombination_id = f.fin_asset_acct)" +
      "      and f.fin_financial_account_id = ?" +
      "      and date(fa.dateacct) <= date(?)";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fin_financial_account_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "resultado");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
