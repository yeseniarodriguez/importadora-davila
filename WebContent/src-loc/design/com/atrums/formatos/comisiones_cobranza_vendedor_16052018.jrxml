<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="comisiones_vendedor" pageWidth="1280" pageHeight="5000" columnWidth="1240" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="0.75"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="FechaD" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="FechaH" class="java.util.Date">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="AD_User_ID" class="java.lang.String"/>
	<parameter name="AUX_AD_User_ID" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{AD_User_ID} == null || $P{AD_User_ID}.equals("")) ? ""  : " and p.em_impr_salesrep_id ='" + $P{AD_User_ID} + "'"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT 	i.documentno AS documento_factura,
	(SELECT name FROM c_bpartner WHERE c_bpartner_id = i.c_bpartner_id) AS tercero,
	(SELECT em_cob_calificacion FROM c_bpartner WHERE c_bpartner_id = i.c_bpartner_id) AS calificacion,
	date(i.dateinvoiced) AS fecha_fact,
	i.totallines AS subtotal,
	i.grandtotal AS total,
	ROUND(coalesce(SUM((SELECT coalesce(sum(total_retencion),0) FROM co_retencion_venta AS rv WHERE rv.c_invoice_id=i.c_invoice_id AND rv.docstatus='CO')),0),2) AS retencion,
	date(p.paymentdate) AS fecha_cobro,
	date(p.paymentdate)-date(i.dateinvoiced) AS Dias,
	p.documentno AS documento_cobro,
	(SUM(dv.paidamt)) as valor_cobrado,
	CASE WHEN (date(i.dateinvoiced) <= date('31/05/2016') or date(i.dateinvoiced) >= date('01/06/2017'))
			THEN (round((sum(dv.paidamt))/1.12,2))
			ELSE (round((sum(dv.paidamt))/1.14,2))
	END as valor_cobrado_sin_iva,
	ROUND(((CASE WHEN (date(i.dateinvoiced) <= date('31/05/2016') or date(i.dateinvoiced) >= date('01/06/2017'))
			THEN (round((sum(dv.paidamt))/1.12,2))
			ELSE (round((sum(dv.paidamt))/1.14,2))
	END)*
	(SELECT CASE
	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 0 AND 30) THEN rango_uno/100
	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 31 AND 60) THEN rango_dos/100
	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 61 AND 90) THEN rango_tres/100
	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 91 AND (CASE WHEN (ic.tipo_cliente = 'CB' OR ic.tipo_cliente = 'CC') THEN 125 ELSE 155 END)) THEN rango_cuatro/100
	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) > (CASE WHEN (ic.tipo_cliente = 'CB' OR ic.tipo_cliente = 'CC') THEN 125 ELSE 155 END)) THEN rango_cinco/100
	END
	FROM impr_comision AS ic
	WHERE 	ic.tipo_comercial = (SELECT CASE WHEN (cb.isemployee = 'Y' AND cb.issalesrep = 'Y') THEN 'CCE' ELSE CASE WHEN (cb.issalesrep = 'Y') THEN 'CCC' ELSE '' END END
					FROM c_bpartner AS cb
					     INNER JOIN ad_user AS au ON (cb.c_bpartner_id = au.c_bpartner_id)
					WHERE au.c_bpartner_id = p.em_impr_salesrep_id
					LIMIT 1)
		AND ic.tipo_cliente = ('C' || (SELECT em_cob_calificacion FROM c_bpartner WHERE c_bpartner_id = i.c_bpartner_id))
		AND ic.tipo_comision = 'COC'
		--AND ic.isactive = 'Y'
		AND date(i.dateinvoiced) between date(ic.fecha_inicio) and date(ic.fecha_fin) --25/10/2017
		LIMIT 1)),2) AS comision_ganada,
	(SELECT bp.name
	FROM c_bpartner bp
	WHERE bp.c_bpartner_id = p.em_impr_salesrep_id) AS Vendedor,
	(SELECT dt.name FROM c_doctype dt WHERE dt.c_doctype_id=i.c_doctype_id) AS type_doc, i.c_doctype_id,
	(SELECT isemployee FROM c_bpartner WHERE c_bpartner_id = p.em_impr_salesrep_id LIMIT 1 ) AS empleado,
	ROUND(((SELECT CASE
	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) > (CASE WHEN (ic.tipo_cliente = 'CB' OR ic.tipo_cliente = 'CC') THEN 125 ELSE 155 END)) THEN 1
	ELSE 0
	END
	FROM impr_comision AS ic
	WHERE 	ic.tipo_comercial = (SELECT CASE WHEN (cb.isemployee = 'Y' AND cb.issalesrep = 'Y') THEN 'CCE' ELSE CASE WHEN (cb.issalesrep = 'Y') THEN 'CCC' ELSE '' END END
					FROM c_bpartner AS cb
					     INNER JOIN ad_user AS au ON (cb.c_bpartner_id = au.c_bpartner_id)
					WHERE au.c_bpartner_id = p.em_impr_salesrep_id
					LIMIT 1)
		AND ic.tipo_cliente = ('C' || (SELECT em_cob_calificacion FROM c_bpartner WHERE c_bpartner_id = i.c_bpartner_id))
		AND ic.tipo_comision = 'COF'
		--AND ic.isactive = 'Y'
		AND date(i.dateinvoiced) between date(ic.fecha_inicio) and date(ic.fecha_fin) --25/10/2017
		LIMIT 1)
	* ((CASE WHEN (date(i.dateinvoiced) <= date('31/05/2016') or date(i.dateinvoiced) >= date('01/06/2017'))
			THEN (round((sum(dv.paidamt))/1.12,2))
			ELSE (round((sum(dv.paidamt))/1.14,2))
	END)*
	(SELECT rango_cuatro/100
	FROM impr_comision AS ic
	WHERE 	ic.tipo_comercial = (SELECT CASE WHEN (cb.isemployee = 'Y' AND cb.issalesrep = 'Y') THEN 'CCE' ELSE CASE WHEN (cb.issalesrep = 'Y') THEN 'CCC' ELSE '' END END
					FROM c_bpartner AS cb
					     INNER JOIN ad_user AS au ON (cb.c_bpartner_id = au.c_bpartner_id)
					WHERE au.c_bpartner_id = p.em_impr_salesrep_id
					LIMIT 1)
		AND ic.tipo_cliente = ('C' || (SELECT em_cob_calificacion FROM c_bpartner WHERE c_bpartner_id = i.c_bpartner_id))
		AND ic.tipo_comision = 'COC'
		--AND ic.isactive = 'Y'
		AND date(i.dateinvoiced) between date(ic.fecha_inicio) and date(ic.fecha_fin) --25/10/2017
		LIMIT 1))),2) AS comision_perdida,
      ROUND((SELECT CASE
            	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 0 AND 30) THEN rango_uno
            	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 31 AND 60) THEN rango_dos
            	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 61 AND 90) THEN rango_tres
            	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) BETWEEN 91 AND (CASE WHEN (ic.tipo_cliente = 'CB' OR ic.tipo_cliente = 'CC') THEN 125 ELSE 155 END)) THEN rango_cuatro
            	WHEN ((date(p.paymentdate)-date(i.dateinvoiced)) > (CASE WHEN (ic.tipo_cliente = 'CB' OR ic.tipo_cliente = 'CC') THEN 125 ELSE 155 END)) THEN rango_cinco
                else 0
            	END
            	FROM impr_comision AS ic
                WHERE 	ic.tipo_comercial = (SELECT CASE WHEN (cb.isemployee = 'Y' AND cb.issalesrep = 'Y') THEN 'CCE' ELSE CASE WHEN (cb.issalesrep = 'Y') THEN 'CCC' ELSE '' END END
                                FROM c_bpartner AS cb
                                     INNER JOIN ad_user AS au ON (cb.c_bpartner_id = au.c_bpartner_id)
                                WHERE au.c_bpartner_id = p.em_impr_salesrep_id
                                LIMIT 1)
                    AND ic.tipo_cliente = ('C' || (SELECT em_cob_calificacion FROM c_bpartner WHERE c_bpartner_id = i.c_bpartner_id))
                    AND ic.tipo_comision = 'COC'
					--AND ic.isactive = 'Y'
                    AND date(i.dateinvoiced) between date(ic.fecha_inicio) and date(ic.fecha_fin) --25/10/2017
                    LIMIT 1),2) || '%' AS porcentaje
FROM fin_payment p
     INNER JOIN fin_payment_detail_v AS dv ON (p.fin_payment_id = dv.fin_payment_id AND dv.fin_paymentmethod_id NOT IN ('5D66B495BC9B47FE919D8ABE1F71458D'))
     INNER JOIN fin_payment_schedule AS ps ON (dv.fin_payment_sched_inv_id = ps.fin_payment_schedule_id)
     INNER JOIN c_invoice AS i ON (ps.c_invoice_id = i.c_invoice_id)
     INNER JOIN c_doctype AS dt ON (p.c_doctype_id = dt.c_doctype_id)
     INNER JOIN c_doctype_trl AS dtr ON (dt.c_doctype_id = dtr.c_doctype_id)
     INNER JOIN c_doctype dti ON (i.c_doctype_id = dti.c_doctype_id)
WHERE dtr.ad_language = 'es_EC'
	AND p.processed = 'Y'
	AND p.fin_financial_account_id NOT IN (SELECT fin_financial_account_id
                           FROM fin_financial_account
                           WHERE name LIKE '%etenci%')
	--AND p.status = 'RDNC'
	AND p.em_impr_salesrep_id IS NOT NULL
	AND p.isreceipt = 'Y'
    and dti.em_co_tp_comp_autorizador_sri in ('1','18')
	AND p.fin_payment_id IN (SELECT fin_payment_id FROM fin_payment_detail_v WHERE fin_payment_sched_inv_id IS NOT NULL)
	AND date(p.paymentdate) >= date($P{FechaD})
	AND date(p.paymentdate) <= date($P{FechaH})
	AND date(i.dateinvoiced) >= date('01/01/2017')
$P!{AUX_AD_User_ID}
GROUP BY i.documentno, i.c_bpartner_id, i.dateinvoiced, i.totallines, i.grandtotal, p.paymentdate, p.documentno, p.em_impr_salesrep_id, i.c_doctype_id
ORDER BY 14,15,8;]]>
	</queryString>
	<field name="documento_factura" class="java.lang.String"/>
	<field name="tercero" class="java.lang.String"/>
	<field name="calificacion" class="java.lang.String"/>
	<field name="fecha_fact" class="java.sql.Date"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="retencion" class="java.math.BigDecimal"/>
	<field name="fecha_cobro" class="java.sql.Date"/>
	<field name="dias" class="java.lang.Integer"/>
	<field name="documento_cobro" class="java.lang.String"/>
	<field name="valor_cobrado" class="java.math.BigDecimal"/>
	<field name="valor_cobrado_sin_iva" class="java.math.BigDecimal"/>
	<field name="comision_ganada" class="java.math.BigDecimal"/>
	<field name="vendedor" class="java.lang.String"/>
	<field name="type_doc" class="java.lang.String"/>
	<field name="comision_perdida" class="java.math.BigDecimal"/>
	<field name="porcentaje" class="java.lang.String"/>
	<variable name="subtotal" class="java.math.BigDecimal" resetType="Group" resetGroup="type_doc" calculation="Sum">
		<variableExpression><![CDATA[$F{comision_ganada}]]></variableExpression>
	</variable>
	<variable name="total" class="java.math.BigDecimal" resetType="Group" resetGroup="vendedor" calculation="Sum">
		<variableExpression><![CDATA[$F{comision_ganada}]]></variableExpression>
	</variable>
	<variable name="subtotal_perd" class="java.math.BigDecimal" resetType="Group" resetGroup="type_doc" calculation="Sum">
		<variableExpression><![CDATA[$F{comision_perdida}]]></variableExpression>
	</variable>
	<variable name="total_perd" class="java.math.BigDecimal" resetType="Group" resetGroup="vendedor" calculation="Sum">
		<variableExpression><![CDATA[$F{comision_perdida}]]></variableExpression>
	</variable>
	<group name="vendedor">
		<groupExpression><![CDATA[$F{vendedor}]]></groupExpression>
		<groupHeader>
			<band height="40">
				<textField isBlankWhenNull="true">
					<reportElement style="SubTitle" x="47" y="13" width="511" height="24" forecolor="#CC0000"/>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{vendedor}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement mode="Opaque" x="0" y="13" width="36" height="24" forecolor="#CCCCCC" backcolor="#CCCCCC"/>
				</rectangle>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="32">
				<staticText>
					<reportElement style="Column header" x="896" y="2" width="160" height="30" forecolor="#CC0000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="17" isBold="true"/>
					</textElement>
					<text><![CDATA[Total:]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement mode="Transparent" x="1057" y="2" width="79" height="30" forecolor="#CC0000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="Arial" size="17" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{total}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement mode="Transparent" x="1136" y="2" width="79" height="30" forecolor="#CC0000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="Arial" size="17" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{total_perd}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="type_doc">
		<groupExpression><![CDATA[$F{type_doc}]]></groupExpression>
		<groupHeader>
			<band height="30">
				<textField>
					<reportElement style="SubTitle" x="0" y="4" width="557" height="20"/>
					<textElement>
						<font size="16" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{type_doc}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="30">
				<staticText>
					<reportElement style="Column header" x="897" y="0" width="160" height="30"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="17" isBold="true"/>
					</textElement>
					<text><![CDATA[SubTotal:]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement mode="Transparent" x="1057" y="0" width="80" height="30" forecolor="#666666" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="Arial" size="17" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{subtotal}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement mode="Transparent" x="1137" y="0" width="80" height="30" forecolor="#666666" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="Arial" size="17" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{subtotal_perd}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="70" splitType="Stretch">
			<staticText>
				<reportElement style="Title" x="0" y="13" width="536" height="33"/>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Informe de Comisiones por Cobranza]]></text>
			</staticText>
			<image hAlign="Left" isUsingCache="true" onErrorType="Icon">
				<reportElement x="1069" y="0" width="150" height="69" isPrintInFirstWholeBand="true"/>
				<imageExpression class="java.awt.Image"><![CDATA[org.openbravo.erpCommon.utility.Utility.showImageLogo("yourcompanylogin")]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="54" splitType="Stretch">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="49" width="1219" height="2"/>
				<graphicElement>
					<pen lineWidth="0.5" lineColor="#999999"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement style="Column header" x="0" y="19" width="45" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[# Fac]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="50" y="19" width="225" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Tercero]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="276" y="19" width="40" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Calif]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="316" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Fecha Factura]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="397" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Subtotal]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="477" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="557" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Retención]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="638" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Fecha Cobro]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="719" y="19" width="50" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Días Cartera]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="769" y="19" width="50" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[# Cobro]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="899" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Valor Cobrado sin IVA]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="1057" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Comision Ganada]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="831" y="19" width="60" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Valor Cobrado]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="1137" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Comision Perdida]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="977" y="19" width="80" height="30"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="11"/>
				</textElement>
				<text><![CDATA[Porcentaje Comision]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<textField>
				<reportElement style="Detail" x="0" y="0" width="45" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{documento_factura}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="48" y="0" width="225" height="15"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{tercero}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="Detail" x="277" y="0" width="40" height="15"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{calificacion}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="317" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{fecha_fact}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="398" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{subtotal}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="478" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="558" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{retencion}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="718" y="0" width="50" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{dias}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="768" y="0" width="50" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{documento_cobro}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="818" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{valor_cobrado}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="898" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{valor_cobrado_sin_iva}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="Detail" x="1056" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{comision_ganada}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="638" y="0" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.util.Date"><![CDATA[$F{fecha_cobro}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement style="Detail" x="1137" y="1" width="80" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{comision_perdida}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="977" y="0" width="79" height="14"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{porcentaje}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement style="Column header" x="940" y="0" width="80" height="20"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="1020" y="0" width="40" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="EEEEE dd MMMMM yyyy">
				<reportElement style="Column header" x="0" y="0" width="197" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
