package com.atrums.formatos.erpReports;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

public class Rpt_Buffer extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strwarehouseoId = vars.getSessionValue("Rpt_Equipo.inpmWarehouseId");
      String strporcentaje = "0";

      if (strwarehouseoId.equals(""))
        strwarehouseoId = vars.getSessionValue("inpmWarehouseId");
      if (strwarehouseoId.equals(""))
        strwarehouseoId = vars.getStringParameter("inpmWarehouseId");

      if (strwarehouseoId.equals(""))
        strwarehouseoId = vars.getStringParameter("inpmWarehouseId");

      strporcentaje = vars.getStringParameter("inpemImprPorcentaje");

      if (strporcentaje.equals("")) {
        strporcentaje = "0";
      }

      if (log4j.isDebugEnabled()) {
        log4j.debug("strWarehouseId" + strwarehouseoId);
        log4j.debug("strporcentaje" + strporcentaje);
      }

      printPagePartePDF(response, vars, strwarehouseoId, strporcentaje);
    } else
      pageError(response);
  }

  private void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strwarehouseoId, String strporcentaje) throws ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: xml");
    // JasperPrint jasperPrint;

    String strReportName = "@basedesign@/com/atrums/formatos/erpReports/Rpt_Buffer.jrxml";

    response.setHeader("Content-disposition", "inline; filename=Rpt_Equipo.pdf");

    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("DOCUMENT_ID", strwarehouseoId);
    parameters.put("aument_porcen", strporcentaje);

    renderJR(vars, response, strReportName, "xls", parameters, null, null);
  }

  public String getServletInfo() {
    return "Servlet that presents the RptCOrders seeker";
  } // End of getServletInfo() method
}
