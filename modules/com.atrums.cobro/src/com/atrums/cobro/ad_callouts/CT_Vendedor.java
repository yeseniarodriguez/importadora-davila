package com.atrums.cobro.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.xmlEngine.XmlDocument;

public class CT_Vendedor extends HttpSecureAppServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strCBpartnerId = vars.getStringParameter("inpcBpartnerId");
      String strIsReceipt = vars.getStringParameter("inpisreceipt");

      try {
        printPage(response, vars, strCBpartnerId, strIsReceipt);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strCBpartnerId, String strIsReceipt) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    BusinessPartner bpartner = OBDal.getInstance().get(BusinessPartner.class, strCBpartnerId);
    boolean isReceipt = "Y".equals(strIsReceipt);

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/atrums/cobro/ad_callouts/CallOut")
        .createXmlDocument();

    StringBuffer result = new StringBuffer();

    result.append("var calloutName='CT_Vendedor';\n\n");
    result.append("var respuesta = new Array(");

    CTVendedorData[] data = CTVendedorData.select(this, strCBpartnerId);

    if (data.length > 0) {
      result.append("new Array(\"inpemImprSalesrepId\", \"" + data[0].salesrepId + "\")");
    }

    try {
      result.append(",new Array(\"inpfinPaymentmethodId\", \"" + (isReceipt
          ? bpartner.getPaymentMethod().getId() : bpartner.getPOPaymentMethod().getId()) + "\")");
      result.append(",new Array(\"inpfinFinancialAccountId\", \""
          + (isReceipt ? bpartner.getAccount().getId() : bpartner.getPOFinancialAccount().getId())
          + "\")");
    } catch (Exception e) {
      log4j.info("No default info for the selected business partner");
    }

    if ((!strCBpartnerId.equals(""))
        && (FIN_Utility.isBlockedBusinessPartner(strCBpartnerId, "Y".equals(strIsReceipt), 4))) {
      // If the Business Partner is blocked for this document, show an information message.
      result.append(",new Array(\"MESSAGE\", \"" + OBMessageUtils.messageBD("ThebusinessPartner")
          + " " + bpartner.getIdentifier() + " "
          + OBMessageUtils.messageBD("BusinessPartnerBlocked") + "\")");
    }

    result.append(");");

    // inject the generated code
    xmlDocument.setParameter("array", result.toString());

    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
